Source: ruby-debug-inspector
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1)
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-debug-inspector.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-debug-inspector
Homepage: https://github.com/banister/debug_inspector
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-debug-inspector
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: Ruby wrapper for the MRI 2.0 debug_inspector API
 Adds methods to RubyVM::DebugInspector to allow for inspection of backtrace
 frames.
 .
 The debug_inspector C extension and API were designed and built by Koichi
 Sasada, this project is just a gemification of his work.
 .
 This library makes use of the debug inspector API which was added to MRI 2.0.0.
 Only works on MRI 2 and 3. Requiring it on unsupported Rubies will result in a
 no-op.
 .
 Recommended for use only in debugging situations. Do not use this in
 production apps.
